import React, {Component}from 'react';
import {
    StackNavigator 
} from 'react-navigation';
import { View, Dimensions, Image } from 'react-native';

import TabNavigator from 'react-native-tab-navigator';

import Home from './screens/Home';
import User from './screens/User';
import Detail from './screens/Detail';
import Menu from './screens/Menu';
export default class Router extends Component{
    constructor() {
        super()
        this.state = {
            selectedTab: 'home',
        }
    }
    render(){
        return(
            <View style={{flex:1}}>
                
                <TabNavigator tabBarStyle={{ height: 50, overflow: 'hidden' }}>
                <TabNavigator.Item
                    selected={this.state.selectedTab === 'home'}
                    title="Home"
                    renderIcon={() => <Image style={{ width: 25, height: 25 }}  source={require('./icon/home.png')} />}
                    renderSelectedIcon={() => <Image style={{ width: 25, height: 25 }} source={require('./icon/home.png')} />}
                    //badgeText="1"
                    onPress={() => this.setState({ selectedTab: 'home' })}>
                    <Home/>
                </TabNavigator.Item>
                <TabNavigator.Item
                    selected={this.state.selectedTab === 'user'}
                    title="Account"
                    renderIcon={() => <Image style={{ width: 25, height: 25 }} source={require('./icon/home.png')} />}
                 renderSelectedIcon={() => <Image style={{ width: 25, height: 25 }} source={require('./icon/home.png')} />}
                    //renderBadge={() => <CustomBadgeView />}
                    onPress={() => this.setState({ selectedTab: 'user' })}>
                   <User/>
                </TabNavigator.Item>
            </TabNavigator>
            </View>
        )
    }
}
export const HomeStack = StackNavigator ({
    ManHinh_Home:{
        screen: Home
    }
})
