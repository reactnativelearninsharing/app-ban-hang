import React, { Component } from 'react';
import { AppRegistry, View, Text} from 'react-native';
import { DrawerNavigator} from 'react-navigation';
import Icon from "react-native-vector-icons/FontAwesome";
import Login from './components/menuComponents/authentication/Login';
import ChangeInfo from './components/menuComponents/ChangeInfo';


const drawernav = DrawerNavigator({
    DrawerItem1: {
        screen: ChangeInfo,
        navigationOptions: {
            drawer: {
                label: 'Drawer 1',
                icon: ({ tintColor }) => <Icon name="bars" size={24} />
            },
        },
    },
    DrawerItem2: {
        screen: ChangeInfo,
        navigationOptions: {
            drawer: {
                label: 'Drawer 2',
                icon: ({ tintColor }) => <Icon name="bars" size={24} />
            },
        },
    },
});

export default drawernav;
